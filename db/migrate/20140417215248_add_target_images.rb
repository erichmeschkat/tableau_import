class AddTargetImages < ActiveRecord::Migration
  def change
    add_column :dim_content, :target_image_url_thumb, :string
    add_column :dim_content, :target_image_url_large, :string
    add_column :dim_content, :dim_mobile_app_id, :string
  end
end
