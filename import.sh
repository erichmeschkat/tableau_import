
echo "" > ./log/import.log

# Step 1: clear all events from given date
rake tableau:delete_events_day

# Step 2: populate all event data 
# removed unhandled events: iap_purchase crayon_unlock crayon_already_unlocked 
# Events have to be added to fact tables, but currently this is undefined by product team
EVENTS=(email_registration app_installed app_upgraded session_started session_ended target_acquired target_lost experience_started experience_ended share favorite menu_button_pressed outside_activity url_visited video_played error)

for i in ${EVENTS[@]}; do
  EVENT=${i} rake tableau:import_event
done

# Step 3: populate dim table data from 4dstudio DB
rake tableau:populate_from_4ds

rake tableau:send_report

exit 0
