# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
TableauImport::Application.config.secret_key_base = '3f795758038ce2c19ebb180fa1217115d384cb7823020244b32db418afed17cc25a8f5088f2d89f01d15767641c7a8937b04589661912a17213d1b857102fe61'
