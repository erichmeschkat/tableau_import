TableauImport::Application.routes.draw do

  root 'home#index'

  get '/' => 'home#index'

  get '/import' => 'home#import'
  get '/emailcsv' => 'home#emailcsv'
end
