require 'rubygems'
require "active_record"
require 'mixpanel_client'
require 'pg'

require 'fileutils'
require 'open-uri'
require 'yaml'
require 'rollbar'
require 'multi_json'
require 'fog'
require 'rollbar'
require 'active_support/time'

Rollbar.configure do |config|
  config.access_token = '8b0e57e66369433ca1932101dc517048'
end

$config = YAML.load_file('config/import.yml')

$aws = $config['aws']
$aws_connection = Fog::Storage.new(
    provider: 'AWS',
    aws_access_key_id: $aws['access_key'],
    aws_secret_access_key: $aws['secret_key'],
    region: 'us-west-1'
  )

# Email to/from info comes from config.yml:
$sendgrid_username = $config['sendgrid']['sendgrid_username']
$sendgrid_password = $config['sendgrid']['sendgrid_password']
$to = $config['sendgrid']['to']
$cc = $config['sendgrid']['cc']
$toname = $config['sendgrid']['toname']
$from = $config['sendgrid']['from']

namespace :tableau do
  dim_tables = [
      "analytics_version",
      "app_version",
      "application",
      "button",
      "card",
      "carrier",
      "content",
      "date",
      "device_app_interaction",
      "device_os_version",
      "device",
      "enterprise",
      "error",
      "event_type",
      "experience",
      "project",
      "location",
      "ip_address",
      "network",
      "outside_activity",
      "reason_ended",
      "session",
      "share_service",
      "share_type",
      "time_zone",
      "url",
      "video_type",
      "video_service",
      "activity_type"
  ];

  # The value of these fields determine whether the value is new and a new dim table record should be created, or if it exists already
  dim_unique_fields = {
      "analytics_version" => ["dim_analytics_version_id"],
      "app_version" => ["dim_application_id", "version"],
      "application" => ["dim_application_id"],
      "button" => ["button_type", "button_name"],
      "card" => ["dim_card_id"],
      "carrier" => ["carrier_name"],
      "content" => ["dim_content_id"],
      "date" => ["full_date"],
      "device_app_interaction" => ["dim_application_id", "dim_device_id"],
      "device_os_version" => ["device_os_version"],
      "device" => ["distinct_id"],
      "enterprise" => ["dim_enterprise_id"],
      "error" => ["error_code", "error_name"],
      "event_type" => ["event_type"],
      "experience" => ["experience_id", "dim_content_id"],
      "project" => ["dim_project_id"],
      "ip_address" => ["ip_address"],
      "location" => ["city", "state", "country"],
      "network" => ["network"],
      "outside_activity" => ["outside_activity_url"], # Correct?
      "reason_ended" => ["reason_ended"],
      "session" => ["dim_device_id", "session_counter"],
      "share_service" => ["share_service"],
      "share_type" => ["share_type"],
      "time_zone" => ["time_zone"],
      "url" => ["url"],
      "video_type" => ["video_type"],
      "video_service" => ["video_service"],
      "activity_type" => ["activity_type"]
  };

  def log_output(str)
    puts str
    $stdout.flush
    open('log/import.log', 'a') { |f|
      f.puts str + "\n"
    }
  end

  def s3_url(asset, size)
    if size == "original"
      size = ""  # Original has no prefix for the filename
    else
      size = size + "_"
    end
    "https://4dstudio.s3.amazonaws.com/uploads/asset/file/#{asset['id']}/#{size}#{asset['file']}"
  end

  def get_connection_4ds
    config = YAML.load_file('config/database_4ds.yml')
    host = config["host"]
    port = config["port"]
    options = ""
    tty = false
    dbname = config["database"]
    user = config["username"]
    password = config["password"]
    conn = PG::Connection.new(host, port, options, tty, dbname, user, password)
  end

  #logfile = open('stdout.log', 'a')
  #def log(str)
  #  log_output str
  #  logfile { |f|
  #    f.puts str
  #  }    
  #end

  def query(sql)
    ActiveRecord::Base.connection.execute(sql)
  end

  # Inserts hsh_fields into the appropriate dim table, but only if the records unique_fields key doesn't already exist in dim_data
  def insert_dim(tbl, dim_unique_fields, arr_fields, props, dim_data, counts)
    counts["dim_" + tbl] ||= 0

    key = []
    dim_unique_fields[tbl].each do |fld|
      key << props[fld]
    end
    key = key.join("_")
    if dim_data[tbl]
      return dim_data[tbl][key] if dim_data[tbl][key]
    else
      abort "This table DNE in dim_data: " + tbl
    end

    counts["dim_" + tbl] += 1

    fields = ""
    values = ""
    arr_fields.each do |fld|
      fields += "#{fld},"
      val = props[fld]
      val = "#{ActiveRecord::Base.sanitize(val)}"
      val = "NULL" if val == "''"
      values += "#{val},"
    end
    fields = fields.chomp(",")
    values = values.chomp(",")
    sql = "
      INSERT INTO dim_#{tbl}
      (#{fields})
      VALUES (#{values})
      RETURNING dim_#{tbl}_id
    "
    response = query(sql)

    val = response.getvalue(0,0).to_i
    dim_data[tbl][key] = val
    return val
  end

  def process_data(mp_row, counts, dim_unique_fields, dim_data)
    event_num = 0;
    other_count = 0;
    valid_event = 0;

    valid_environments = ["4dstudio.daqri.com", "i4dstudio.daqri.com", "i4ds.daqri.com"];
    event_num += 1

    begin # Catch-all error handler. This should be disabled while debugging
      eventname = mp_row["event"]
      abort mp_row.inspect if eventname == 'debug_identify'
      eventprops = mp_row["properties"]

      # Skip event if environment is set and is not in the list
      if (eventprops["environment"] && !valid_environments.include?(eventprops["environment"]))
        return 0, 0, 0
      end

      if eventprops.include? 'event_time'
        valid_event += 1

        counts["event_" + eventname] ||= 0
        counts["event_" + eventname] += 1

        # These fields are used to create the INSERT call for the current event into the appropriate fact_* table
        fields_to_copy = [
            "event_time",
            "collect_time",
            "dim_application_id",
            "dim_app_version_id",
            "dim_date_id_event",
            "dim_date_id_collect",
            "dim_device_id",
            "dim_event_type_id",
            "dim_ip_address_id",
            "dim_location_id",
            "dim_session_id",
            "dim_time_zone_id",
            "session_counter",
            "latitude",
            "longitude"
        ]

        # Overrides: fields called X in the mixpanel event need to be referred to as Y in our database
        eventprops["dim_application_id"] = eventprops["daqri_app_id"]
        eventprops["application_brand"] = eventprops["daqri_app_brand"]
        eventprops["application_current_version"] = eventprops["daqri_app_version"]
        eventprops["version"] = eventprops["daqri_app_version"]
        # We ignore all events from this version, as the data's messed up
        return 0, 0, 0 if eventprops["version"] == "3.1.0.2"
        eventprops["device_os"] = eventprops["device_os_version"]
        eventprops["city"] = eventprops["$city"]
        eventprops["state"] = eventprops["$region"]
        eventprops["country"] = eventprops["mp_country_code"]
        eventprops["event_type"] = eventname
        eventprops["collect_time"] = Time.at(eventprops["time"]).utc.strftime("%Y-%m-%d %H:%M:%S")
        eventprops["activity_type"] = eventprops["type"]

        # Defaults: set default values if they're unset by mixpanel event
        eventprops["latitude"] = "0.00" if eventprops["latitude"].blank?
        eventprops["longitude"] = "0.00" if eventprops["longitude"].blank?
        eventprops["city"] = "unknown" if eventprops["city"].blank?
        eventprops["state"] = "unknown" if eventprops["state"].blank?
        eventprops["country"] = "unknown" if eventprops["country"].blank?
        eventprops["daqri_app_id"] = eventprops["daqri_app_id"].to_i
        eventprops["time_zone"] = "unknown" if eventprops["time_zone"].blank?

        def get_date_by_timestamp(ts)
          if ts.to_s.match(/^\d+$/)
            Time.at(ts).to_datetime.strftime("%Y%m%d")
          else
            DateTime.parse(ts).strftime("%Y%m%d")
          end
        end

        # Populate dim_* tables with unique instances of the various data types, get back the inserted ID and put it into eventprops
        eventprops["dim_application_id"] = insert_dim("application", dim_unique_fields, [ "dim_application_id", "application_brand", "application_current_version", "enterprise_id" ], eventprops, dim_data, counts)
        eventprops["dim_app_version_id"] = insert_dim("app_version", dim_unique_fields, [ "dim_application_id", "version" ], eventprops, dim_data, counts)
        eventprops["dim_device_id"] = insert_dim("device", dim_unique_fields, [ "distinct_id", "device_os", "device_type" ], eventprops, dim_data, counts)
        eventprops["dim_location_id"] = insert_dim("location", dim_unique_fields, [ "city", "state", "country" ], eventprops, dim_data, counts)
        eventprops["dim_date_id_event"] = get_date_by_timestamp(eventprops["event_time"])
        eventprops["dim_date_id_collect"] = get_date_by_timestamp(eventprops["time"])
        eventprops["dim_event_type_id"] = insert_dim("event_type", dim_unique_fields, [ "event_type" ], eventprops, dim_data, counts)
        eventprops["dim_session_id"] = insert_dim("session", dim_unique_fields, [ "dim_device_id", "session_counter" ], eventprops, dim_data, counts)
        eventprops["dim_ip_address_id"] = insert_dim("ip_address", dim_unique_fields, [ "ip_address" ], eventprops, dim_data, counts)
        eventprops["dim_time_zone_id"] = insert_dim("time_zone", dim_unique_fields, [ "time_zone" ], eventprops, dim_data, counts)


        unless eventprops["content_id"].blank?
          # Override: mixpanel sends target_id in place of content_id
          eventprops["dim_content_id"] = eventprops["content_id"]

          eventprops["dim_content_id"] = insert_dim("content", dim_unique_fields, [ "dim_content_id", "content_name" ], eventprops, dim_data, counts)
          fields_to_copy << "dim_content_id"
        end

        unless eventprops["project_id"].blank?
          # Override: mixpanel sends project_id in place of project_id
          eventprops["dim_project_id"] = eventprops["project_id"]
          eventprops["project_name"] = eventprops["project_project"]

          eventprops["dim_project_id"] = insert_dim("project", dim_unique_fields, [ "dim_project_id", "project_name" ], eventprops, dim_data, counts)
          fields_to_copy << "dim_project_id"
        end

        unless eventprops["experience_id"].blank?
          if eventprops["dim_content_id"].blank?
            #puts "Skipping due to NULL dim_content_id: #{mp_row.inspect}"
            return 0, 0, 0
          end
          eventprops["dim_experience_id"] = insert_dim("experience", dim_unique_fields, [ "dim_content_id", "experience_id", "experience_name", "experience_type" ], eventprops, dim_data, counts)
          fields_to_copy << "dim_experience_id"
        end

        unless eventprops["card_id"].blank?
          eventprops["dim_card_id"] = eventprops["card_id"]
          eventprops["dim_card_id"] = insert_dim("card", dim_unique_fields, [ "dim_card_id", "card_url", "card_type" ], eventprops, dim_data, counts)
          fields_to_copy << "dim_card_id"
        end

        unless eventprops["carrier_name"].blank?
          eventprops["dim_carrier_id"] = insert_dim("carrier", dim_unique_fields, [ "carrier_name" ], eventprops, dim_data, counts)
          fields_to_copy << "dim_carrier_id"
        end

        unless eventprops["analytics_version"].blank?
          eventprops["dim_analytics_version_id"] = insert_dim("analytics_version", dim_unique_fields, [ "analytics_version" ], eventprops, dim_data, counts)
          fields_to_copy << "dim_analytics_version_id"
        end

        case eventname
          when "app_installed", "app_upgraded"
            event_table = "fact_app_event"
          when "session_started", "session_ended", "target_acquired", "target_lost", "experience_started", "experience_ended"
            event_table = "fact_standard_event"
          when "share", "favorite", "menu_button_pressed", "outside_activity", "url_visited", "video_played"
            event_table = "fact_special_event"
          when "error"
            event_table = "fact_error_event"
          else
            # Skip events we dont handle per Frank 12/18/2014
            #puts "Skipping due to unhandled event: #{eventname}"
            return 0, 0, 0; # event_table = "fact_custom_event"
        end

        case eventname
          when "app_installed"
            sql = "
            UPDATE dim_device
            SET
              device_model = #{ActiveRecord::Base.sanitize(eventprops['device_model'])},
              advertiser_id = #{ActiveRecord::Base.sanitize(eventprops['advertiser_id'])},
              vendor_id = #{ActiveRecord::Base.sanitize(eventprops['vendor_id'])},
              device_os = #{ActiveRecord::Base.sanitize(eventprops['device_os_version'])},
              language = #{ActiveRecord::Base.sanitize(eventprops['language'])},
              device_type = #{ActiveRecord::Base.sanitize(eventprops['device_type'])}
            WHERE
              dim_device_id = #{ActiveRecord::Base.sanitize(eventprops['dim_device_id'])}
            "
            query sql

            eventprops["dim_device_os_version_id"] = insert_dim("device_os_version", dim_unique_fields, [ "device_os_version" ], eventprops, dim_data, counts)
            fields_to_copy += [ "dim_device_os_version_id" ]
          when "app_upgraded"
            sql = "
            UPDATE dim_device
            SET
              device_os = #{ActiveRecord::Base.sanitize(eventprops['device_os_version'])},
              language = #{ActiveRecord::Base.sanitize(eventprops['language'])}
            WHERE
              dim_device_id = #{ActiveRecord::Base.sanitize(eventprops['dim_device_id'])}
            "
            query sql

            eventprops["old_version"] = eventprops["oldVersion"]
            eventprops["new_version"] = eventprops["newVersion"]

            eventprops["old_version_id"] = insert_dim("app_version", dim_unique_fields, [ "old_version" ], eventprops, dim_data, counts)
            eventprops["new_version_id"] = insert_dim("app_version", dim_unique_fields, [ "new_version" ], eventprops, dim_data, counts)
            eventprops["dim_device_os_version_id"] = insert_dim("device_os_version", dim_unique_fields, [ "device_os_version" ], eventprops, dim_data, counts)
            fields_to_copy += [ "old_version_id", "new_version_id", "dim_device_os_version_id" ]
          when "session_started"
            # We must update device_model here b/c it's sometimes not there during app_install
            sql = "
            UPDATE dim_device
            SET
              device_model = #{ActiveRecord::Base.sanitize(eventprops['device_model'])}
            WHERE
              dim_device_id = #{ActiveRecord::Base.sanitize(eventprops['dim_device_id'])} AND (device_model = '' OR device_model IS NULL)
            "
            query sql

            # Set first/last seen
            device_app_interaction_id = insert_dim("device_app_interaction", dim_unique_fields, [ "dim_device_id", "dim_application_id" ], eventprops, dim_data, counts)

            sql = "
            UPDATE dim_device_app_interaction
            SET
              first_seen = #{ActiveRecord::Base.sanitize(eventprops['event_time'])}
            WHERE
              dim_device_app_interaction_id = #{ActiveRecord::Base.sanitize(device_app_interaction_id)}
              AND first_seen IS NULL
            "
            query sql

            sql = "
            UPDATE dim_device_app_interaction
            SET
              last_seen = #{ActiveRecord::Base.sanitize(eventprops['event_time'])}
            WHERE
              dim_device_app_interaction_id = #{ActiveRecord::Base.sanitize(device_app_interaction_id)}
            "
            query sql

            eventprops["dim_network_id"] = insert_dim("network", dim_unique_fields, [ "network" ], eventprops, dim_data, counts)

            fields_to_copy += [ "dim_network_id" ]

          when "session_ended"
            fields_to_copy += [ "duration" ]

          when "target_acquired"
            fields_to_copy += [ "target_acquisition_counter" ]

          when "target_lost"
            fields_to_copy += [ "target_acquisition_counter", "duration" ]

          when "experience_started"
            fields_to_copy += [ "target_acquisition_counter", "user_experience_counter", "initiating_chapter" ]

          when "experience_ended"
            eventprops["dim_reason_ended_id"] = insert_dim("reason_ended", dim_unique_fields, [ "reason_ended" ], eventprops, dim_data, counts)
            fields_to_copy += [ "target_acquisition_counter", "user_experience_counter", "destination_chapter", "duration", "dim_reason_ended_id" ]

          when "outside_activity"
            eventprops["dim_outside_activity_id"] = insert_dim("outside_activity", dim_unique_fields, [ "outside_activity_type", "outside_activity_name", "outside_activity_url" ], eventprops, dim_data, counts)
            fields_to_copy += [ "outside_activity_counter", "dim_outside_activity_id", "duration" ]

          when "share"
            eventprops["dim_share_service_id"] = insert_dim("share_service", dim_unique_fields, [ "share_service" ], eventprops, dim_data, counts)
            eventprops["dim_share_type_id"] = insert_dim("share_type", dim_unique_fields, [ "share_type" ], eventprops, dim_data, counts)
            fields_to_copy += [ "dim_share_type_id", "dim_share_service_id", "share_counter" ]

          # This event is not yet collected
          #when "video_played"
          #  fields_to_copy += [ "auto_play", "pause_count", "target_loss_count", "percent_played", "reason_stopped", "duration", "content_id", "project_id", "experience_id" ]

          when "menu_button_pressed"
            eventprops["dim_button_id"] = insert_dim("button", dim_unique_fields, [ "button_name", "button_type" ], eventprops, dim_data, counts)
            fields_to_copy += [ "dim_button_id" ]

          when "url_visited"
            eventprops["dim_url_id"] = insert_dim("url", dim_unique_fields, [ "url" ], eventprops, dim_data, counts)
            eventprops["dim_activity_type_id"] = insert_dim("activity_type", dim_unique_fields, [ "activity_type" ], eventprops, dim_data, counts)
            fields_to_copy += [ "dim_url_id", "dim_activity_type_id" ]

          when "video_played"
            eventprops["dim_url_id"] = insert_dim("url", dim_unique_fields, [ "url" ], eventprops, dim_data, counts)
            eventprops["dim_activity_type_id"] = insert_dim("activity_type", dim_unique_fields, [ "activity_type" ], eventprops, dim_data, counts)
            eventprops["dim_video_service_id"] = insert_dim("video_service", dim_unique_fields, [ "video_service" ], eventprops, dim_data, counts)
            eventprops["dim_video_type_id"] = insert_dim("video_type", dim_unique_fields, [ "video_type" ], eventprops, dim_data, counts)
            fields_to_copy += [ "dim_url_id", "dim_activity_type_id", "dim_video_service_id", "dim_video_type_id" ]

          when "error"
            eventprops["dim_error_id"] = insert_dim("error", dim_unique_fields, [ "error_code", "error_name" ], eventprops, dim_data, counts)
            fields_to_copy += [ "dim_error_id" ]

          when "favorite"
            # We got the card_id above

          else
            other_count += 1
            Rails.logger.error("==== invalid event: #{eventname}")
        end

        fields = values = ""
        fields_to_copy.each do |fld|
          fields += "#{fld},"
          val = eventprops[fld]
          val = "#{ActiveRecord::Base.sanitize(val)}"
          val = "NULL" if val == "''"
          values += "#{val},"
        end

        fields = fields.chomp(",")
        values = values.chomp(",")
        sql = "
          INSERT INTO #{event_table}
          (#{fields})
          VALUES (#{values})
        "
        query(sql)
      end
    rescue Exception => ex
      msg = "ERROR during mixpanel import: " + ex.inspect.slice(0, 1024) + "\n" + ex.backtrace.join("\n").slice(0, 2048)
      log_output msg
      Rails.logger.error(msg)
    end

    return event_num, other_count, valid_event;
  end

  task :debug do
    abort "debug can only be run on development environments" unless Rails.env.development?
    log_output `rake tableau:cleardb`
    log_output `rake tableau:import`
  end

  # Delete and recreate all tables, re-import date data
  task :cleardb => :environment do
    sql = "
      DROP TABLE IF EXISTS fact_app_event;
      DROP TABLE IF EXISTS fact_special_event;
      DROP TABLE IF EXISTS fact_standard_event;
      DROP TABLE IF EXISTS fact_error_event;
      DROP TABLE IF EXISTS dim_analytics_version;
      DROP TABLE IF EXISTS dim_app_version;
      DROP TABLE IF EXISTS dim_application;
      DROP TABLE IF EXISTS dim_button;
      DROP TABLE IF EXISTS dim_card;
      DROP TABLE IF EXISTS dim_carrier;
      DROP TABLE IF EXISTS dim_content;
      DROP TABLE IF EXISTS dim_date;
      DROP TABLE IF EXISTS dim_device_app_interaction;
      DROP TABLE IF EXISTS dim_device_os_version;
      DROP TABLE IF EXISTS dim_device;
      DROP TABLE IF EXISTS dim_enterprise;
      DROP TABLE IF EXISTS dim_error;
      DROP TABLE IF EXISTS dim_event_type;
      DROP TABLE IF EXISTS dim_experience;
      DROP TABLE IF EXISTS dim_project;
      DROP TABLE IF EXISTS dim_ip_address;
      DROP TABLE IF EXISTS dim_location;
      DROP TABLE IF EXISTS dim_network;
      DROP TABLE IF EXISTS dim_outside_activity;
      DROP TABLE IF EXISTS dim_reason_ended;
      DROP TABLE IF EXISTS dim_share_service;
      DROP TABLE IF EXISTS dim_share_type;
      DROP TABLE IF EXISTS dim_session;
      DROP TABLE IF EXISTS dim_time_zone;
      DROP TABLE IF EXISTS dim_url;
      DROP TABLE IF EXISTS dim_user;
      DROP TABLE IF EXISTS dim_video_type;
      DROP TABLE IF EXISTS dim_video_service;
      DROP TABLE IF EXISTS dim_activity_type;
    "
    query(sql)

    log_output "== tables successfully dropped"

    sql = File.read('./sql_create_dim_tables.sql')
    query sql

    sql = File.read('./sql_create_fact_tables.sql')
    query sql
    log_output "== tables successfully created"

    # Populate dates table
    counter = 1
    file = File.new("db/populate_dim_date.sql", "r")
    while (line = file.gets)
      query(line)
    end
    file.close

    log_output "== dim_date successfully populated"
  end

  def mem_use
    pid, size = `ps ax -o pid,rss | grep -E "^[[:space:]]*#{$$}"`.strip.split.map(&:to_i)
    return size.to_s + "Kb"
  end

  task :delete_events_day, [:arg1] => :environment do |t, args|
    start_time = (Time.now.beginning_of_day - 1.day).strftime("%Y-%m-%d")
    end_time   = (Time.now.beginning_of_day - 1.second).strftime("%Y-%m-%d")

    # Override: get all events between a certain range
    if ENV['IMPORT_START'] && ENV['IMPORT_END']
      start_time = ENV['IMPORT_START']
      end_time = ENV['IMPORT_END']
    end

    start_time_sql = Date.parse(start_time).strftime('%Y-%m-%d %H:%M:%S')
    end_time_sql = (Date.parse(end_time) + 1.day - 1.second).strftime('%Y-%m-%d %H:%M:%S')

    log_output "== Deleting from #{start_time_sql} to #{end_time_sql}."
    sql = "
      DELETE FROM fact_app_event WHERE collect_time >= '#{start_time_sql}' AND collect_time <= '#{end_time_sql}';
      DELETE FROM fact_special_event WHERE collect_time >= '#{start_time_sql}' AND collect_time <= '#{end_time_sql}';
      DELETE FROM fact_standard_event WHERE collect_time >= '#{start_time_sql}' AND collect_time <= '#{end_time_sql}';
      DELETE FROM fact_error_event WHERE collect_time >= '#{start_time_sql}' AND collect_time <= '#{end_time_sql}';
    "
    query sql

  end

  task :import_event, [:arg1] => :environment do |t, args|

    event_to_process = ENV['EVENT']

    start_time = (Time.now.beginning_of_day - 1.day).strftime("%Y-%m-%d")
    end_time   = (Time.now.beginning_of_day - 1.second).strftime("%Y-%m-%d")

    # Override: get all events between a certain range
    if ENV['IMPORT_START']
      start_time = ENV['IMPORT_START']
      end_time = ENV['IMPORT_START']
    end

    start_time_sql = Date.parse(start_time).strftime('%Y-%m-%d %H:%M:%S')
    end_time_sql = (Date.parse(end_time) + 1.day - 1.second).strftime('%Y-%m-%d %H:%M:%S')

    import_start = Time.now
    counts = {}

    log_output "================ IMPORT START: #{start_time} - #{end_time} =================="

    config = { api_key: '74a1a6cff2f3e7f9fd01576fa68934e6', api_secret: 'bd9a17228b4be2a223d4842dd965acc6' }
    client = Mixpanel::Client.new(config)

    # Load contents of all DIM tables into memory for quick de-dupe
    
    log_output "== Loading DIM data into memory. #{(Time.now - import_start)} elapsed."
    dim_data = {}
    dim_tables.each do |tbl|
      dim_data[tbl] ||= {}

      tbl_name = "dim_#{tbl}"
      id_name = "dim_#{tbl}_id"

      unique_fields = dim_unique_fields[tbl]
      fields = unique_fields.join(", ")

      # Populate dim_data with the unique values already present in the database.
      # When we encounter mixpanel events, we'll use this to check to see if something should be inserted into a dim table or now.
      sql = "
        SELECT #{fields}, #{id_name}
        FROM #{tbl_name}
        "
      results = query(sql)
      results.each_row do |arr|
        key = arr.slice(0, arr.length - 1).join("_")
        dim_data[tbl][key] = arr[arr.length - 1]
      end
    end

    log_output "== Begin importing mixpanel events into the DB. #{(Time.now - import_start)} elapsed."
    event_num = 0
    other_count = 0
    valid_event = 0
    total_count = 0

    url = client.request_uri('export', {
      from_date: start_time,
      to_date: end_time,
      event: [ event_to_process ]
    })

    log_output "Downloading events json to disk for '#{event_to_process}'\n#{url}"
    `wget -q -O out.json '#{url}'`
    log_output "...done."

    # Example event: 
    # {"event":"error","properties":{"time":1427932808,"distinct_id":"dc7a4eb08ce91c0fa8fd476384117bff","$region":"Florida","analytics_version":"1.0.1","daqri_app_brand":"Crayola","daqri_app_id":86,"daqri_app_version":"1.2.0","daqri_core_version":"3.2.0","device_os_version":"Android OS 5.0.1 / API-21 (LRX22C/464190.4)","device_type":"Android","environment":"4dstudio.daqri.com","error_code":"Error","error_name":"Error: System.IO.IOException: File Download: Handle Client Download File Completed: https://4dstudio.s3.amazonaws.com/uploads/asset/file/51480/P7_Barbie07_GroundPlane_Android_4.5.unity3d with error\"Error getting response stream (Write: The authentication","event_name":null,"event_time":"2015-04-02T00:00:08","ip_address":"172.56.4.125","mp_country_code":"US","session_counter":1,"session_id":"af8cbb7f-5981-4927-ba2f-4f6826695fda","time_zone":"AMT"}}
    log_output "== Begin loading events from json file. #{(Time.now - import_start)} elapsed."
    f = File.open("out.json", "r")
    f.each_line do |line|
      total_count += 1
      en, other, valid = process_data(MultiJson.load(line), counts, dim_unique_fields, dim_data);
      if total_count % 1000 == 0
        log_output "Total events imported so far: #{total_count}"
      end
      event_num += en
      other_count += other
      valid_event += valid
    end
    f.close

    log_output "================ TOTALS: #{event_to_process}=================="
    log_output "Time in seconds: " + (Time.now - import_start).to_s
    log_output "Events: " + event_num.to_s
    log_output "Valid Events: " + valid_event.to_s
    log_output "Unrecognized Events: " + other_count.to_s
    log_output "Events Pulled from MP: " + total_count.to_s
    log_output "================= END: #{event_to_process} ====================\n\n"
  end


  task :populate_from_4ds, [:arg1] => :environment do |t, args|
    log_output "=== Populating from 4ds"
    log_output "++ Memory use 01: #{mem_use()}"
 
    # STEP 2: read from 4d Studio database
    conn = get_connection_4ds
    populate_start = Time.now

    begin # Catch-all error handler for 4dstudio hookup. Should be disabled while debugging

      # 2.1: Populate enterprise
      log_output "== Populating: dim_enterprise."

      counts = {}
      conn.exec( "SELECT * FROM enterprises" ) do |result|
        query( "DELETE FROM dim_enterprise;" )
        result.each do |row|

          counts["4ds_enterprises"] ||= 0
          counts["4ds_enterprises"] += 1

          filled_seats = 1
          sql = "SELECT count(id) as cnt FROM enterprise_members WHERE enterprise_id = #{row['id']}"
          conn.exec( sql ) do |result2|
            result2.each do |row2|
              filled_seats = row2['cnt']
            end
          end

          sql = "
            INSERT INTO dim_enterprise
            (dim_enterprise_id, enterprise_name, tier, seats, storage_current_usage, filled_seats, tableau_username)
            VALUES
            (
              #{ActiveRecord::Base.sanitize(row['id'])},
              #{ActiveRecord::Base.sanitize(row['name'].slice(0, 45))},
              #{ActiveRecord::Base.sanitize(row['tier'])},
              #{ActiveRecord::Base.sanitize(row['seats'])},
              #{ActiveRecord::Base.sanitize(row['storage_current_usage'])},
              #{ActiveRecord::Base.sanitize(row['filled_seats'])},
              #{ActiveRecord::Base.sanitize(row['tableau_username'])}
            )
            "
          query(sql)
        end
      end
      log_output "++ Memory use 04: #{mem_use()}"

      # 2.2: Populate application
      log_output "== Populating: dim_application."
      conn.exec( "SELECT * FROM mobile_apps" ) do |result|
        result.each do |row|
          counts["4ds_mobile_apps"] ||= 0
          counts["4ds_mobile_apps"] += 1

          sql = "
            UPDATE dim_application
            SET
              enterprise_id = #{ActiveRecord::Base.sanitize(row['enterprise_id'])},
              application_name = #{ActiveRecord::Base.sanitize(row['name'])}
            WHERE
              dim_application_id = #{row['id']}
            "
          query(sql)
        end
      end
      log_output "++ Memory use 05: #{mem_use()}"

      # 2.3: Populate projects
      log_output "== Populating: dim_project."
      conn.exec( "SELECT * FROM projects" ) do |result|
        result.each do |row|
          counts["4ds_projects"] ||= 0
          counts["4ds_projects"] += 1

          sql = "
            UPDATE dim_project
            SET
              project_name = #{ActiveRecord::Base.sanitize(row['name'])},
              dim_enterprise_id = #{ActiveRecord::Base.sanitize(row['enterprise_id'])},
              dim_application_id = #{ActiveRecord::Base.sanitize(row['mobile_app_id'])}
            WHERE
              dim_project_id = #{row['id']}
            "
          query(sql)
        end
      end
      log_output "++ Memory use 06: #{mem_use()}"

      # 2.4: Populate contents + experiences
      log_output "== Populating: dim_content."
      contents_all = {}
      conn.exec( "SELECT * FROM contents" ) do |result|
        result.each do |row|
          contents_all[row['id']] = row

          counts["4ds_contents"] ||= 0
          counts["4ds_contents"] += 1

          num_chapters = 0
          regions = MultiJson.load(row['regions'])
          if regions["versions"].blank?
            log_output "Invalid content.regions: #{row['id']}"
            next
          end
          versions = regions["versions"]
          version = versions[0]
          chapters = version["chapters"]
          chapters.each do |chapter|
            if chapter["experienceKeys"].length > 0
              num_chapters += 1
            end
          end
          num_experiences = version["experiences"].length

          target_image_url_thumb = target_image_url_large = nil
          
          if row['target_image_id']
            conn.exec( "SELECT * FROM assets WHERE id = '#{row['target_image_id']}'" ) do |result_asset|
              result_asset.each do |row_asset|
                target_image_url_thumb = s3_url(row_asset, 'thumb')
                target_image_url_large = s3_url(row_asset, 'show')
              end
            end
          end

          dim_application_id = row['mobile_app_id']
          dim_project_id = row['project_id']
          dim_enterprise_id = row['enterprise_id']

          sql = "
            UPDATE dim_content
            SET
              content_name = #{ActiveRecord::Base.sanitize(row['name'])},
              dim_project_id = #{ActiveRecord::Base.sanitize(dim_project_id)},
              dim_application_id = #{ActiveRecord::Base.sanitize(dim_application_id)},
              extended_tracking = #{ActiveRecord::Base.sanitize(row['extended_tracking'] == 'true' ? 'y' : 'n')},
              dim_enterprise_id = #{ActiveRecord::Base.sanitize(dim_enterprise_id)},
              template_type = #{ActiveRecord::Base.sanitize(row['template_type'])},
              target_id = #{ActiveRecord::Base.sanitize(row['target_image_id'])},
              rating = #{ActiveRecord::Base.sanitize(row['rating'])},
              num_chapters = #{num_chapters},
              num_experiences = #{num_experiences},
              target_image_url_thumb = #{ActiveRecord::Base.sanitize(target_image_url_thumb)},
              target_image_url_large = #{ActiveRecord::Base.sanitize(target_image_url_large)}
            WHERE
              dim_content_id = #{row['id']}
            "
          query(sql)

          versions.each do version
            version["experiences"].each do |experience_id, experience|
              sql = "
                UPDATE dim_experience
                SET
                  experience_name = #{ActiveRecord::Base.sanitize(experience['name'])},
                  experience_type = #{ActiveRecord::Base.sanitize(experience['type'])},
                  experience_image_url = #{ActiveRecord::Base.sanitize(experience['image_url'])},
                  experience_video_url = #{ActiveRecord::Base.sanitize(experience['experience_video_url'])},
                  dim_application_id = #{ActiveRecord::Base.sanitize(dim_application_id)},
                  dim_project_id = #{ActiveRecord::Base.sanitize(dim_project_id)},
                  dim_enterprise_id = #{ActiveRecord::Base.sanitize(dim_enterprise_id)}
                WHERE
                  dim_content_id = #{row['id']} AND experience_id = '#{experience_id}'
                "
              query(sql)
            end
          end
        end
      end
      log_output "++ Memory use 07: #{mem_use()}"

      # 2.5: Populate cards
      log_output "== Populating: dim_card."
      conn.exec( "SELECT * FROM cards" ) do |result|
        result.each do |row|
          counts["4ds_cards"] ||= 0
          counts["4ds_cards"] += 1

          card_url = "http://4dstudio.daqri.com/mobile/v1/card/#{row['id']}/html/desktop"

          project_id = nil
          mobile_app_id = nil
          experience_id = row['experience_id']
          if row['content_id']
            content = contents_all[row['content_id']]
            if content
              project_id = content['project_id']
              mobile_app_id = content['mobile_app_id']
            end
          end

          sql = "
            UPDATE dim_card
            SET
              card_type = #{ActiveRecord::Base.sanitize(row['card_type'])},
              card_title = #{ActiveRecord::Base.sanitize(row['title'])},
              card_url = #{ActiveRecord::Base.sanitize(card_url)},
              dim_project_id = #{ActiveRecord::Base.sanitize(project_id)},
              dim_content_id = #{ActiveRecord::Base.sanitize(row['content_id'])},
              dim_experience_id = #{ActiveRecord::Base.sanitize(experience_id)},
              dim_application_id = #{ActiveRecord::Base.sanitize(mobile_app_id)}
            WHERE
              dim_card_id = #{row['id']}
            "
          query(sql)
        end
      end
      log_output "++ Memory use 08: #{mem_use()}"

      # 2.6: Populate users
      log_output "== Populating: dim_user."

      # We clear and rebuild this each time
      query("DELETE FROM dim_user")
      conn.exec( "SELECT * FROM users" ) do |result|
        result.each do |row|
          counts["4ds_users"] ||= 0
          counts["4ds_users"] += 1

          fields = {
            "id" => "dim_user_id",
            "name" => "name",
            "title" => "title",
            "department" => "department",
            "phone" => "phone",
            "enterprise_id" => "dim_enterprise_id",
            "created_at" => "created_at",
            "updated_at" => "updated_at",
            "avatar" => "avatar",
            "project_id" => "dim_project_id",
            "created_by" => "created_by",
            "eula_accepted" => "eula_accepted",
            "sign_in_count" => "sign_in_count"
          }

          sql = "
            INSERT INTO dim_user
          "
          sql += "("
          fields.keys.each do |key|
            sql += fields[key] + ", "
          end
          sql = sql.gsub(/, $/, "")
          sql += ") VALUES ("
          fields.keys.each do |key|
            sql += ActiveRecord::Base.sanitize(row[key]) + ", "
          end
          sql = sql.gsub(/, $/, "")
          sql += ")"

          query(sql)
        end
      end
    rescue Exception => ex
      msg = "ERROR during mixpanel import: " + ex.inspect.slice(0, 1024) + "\n" + ex.backtrace.join("\n").slice(0, 2048)
      log_output msg
      Rails.logger.error(msg)
    end

    log_output "++ Memory use 07: #{mem_use()}"
    log_output "Counts: "

    counts.keys.sort.each do |key|
      log_output key + ": " + counts[key].to_s
    end

    log_output "++ Memory use 09: #{mem_use()}"
    log_output "----- End populated with data from 4ds in "+ (Time.now - populate_start).to_s + " seconds -----"   
  end

  # @return {Hash} a MixPanel formatted object for making fake data
  def mock_mp_event(event, event_time, time, app_id, content_id, duration, share_service, device_id, device_type)
    return {
      "event" => event,
      "properties" => {
        "event_time" => event_time,
        "daqri_app_id" => app_id,
        "time" => time,
        "content_id" => content_id,
        "duration" => duration,
        "share_service" => share_service,
        "distinct_id" => device_id,
        "device_type" => device_type
      }
    };
  end

  # creates mock data for a local development environment
  task(:mock, {[:arg1] => :environment}) {
    if (!Rails.env.development? && !Rails.env.test?)
      abort("mock can only be run on development environments!");
    end
    puts(`rake tableau:cleardb`);
    dim_data = {};
    dim_tables.each { |table|
      dim_data[table] = {};
    }
    now = Time.now();
    now_time = now.strftime("%Y-%m-%d %H:%M:%S");
    now_ms = now.to_i() + now.utc_offset();

    def rand_device_id()
      return "id_#{rand(10000)}";
    end
    def rand_device_type()
      device_types = ["iPhone", "Android", "iPad", "LCARS"];
      return device_types[rand(device_types.length())];
    end
    mock_mp_data = [];
    100.times() {
      device_id = rand_device_id();
      device_type = rand_device_type();
      mock_mp_data << mock_mp_event("session_started", now_time, now_ms, 1, nil, nil, nil, device_id, device_type);
    }
    for content_id in 1..20 do
      for target_count in 1..100 do
        duration = 60 * rand();
        device_id = rand_device_id();
        device_type = rand_device_type();
        mock_mp_data << mock_mp_event("target_acquired", now_time, now_ms, 1, content_id, nil,      nil, device_id, device_type);
        mock_mp_data << mock_mp_event("target_lost",     now_time, now_ms, 1, content_id, duration, nil, device_id, device_type);
      end
    end
    share_services = ["facebook", "twitter", "pinterest", "email"];
    20.times() {
      device_id = rand_device_id();
      device_type = rand_device_type();
      share_service = share_services[rand(share_services.length())];
      mock_mp_data << mock_mp_event("share", now_time, now_ms, 1, 1, nil, share_service, device_id, device_type);
      mock_mp_data << mock_mp_event("share", now_time, now_ms, 1, 2, nil, share_service, device_id, device_type);
      mock_mp_data << mock_mp_event("share", now_time, now_ms, 1, 3, nil, share_service, device_id, device_type);
    }
    process_data(mock_mp_data, {}, dim_unique_fields, dim_data);
    puts("mock data successfully populated");
  }


  def upload_s3 bucket, key, source, is_public=false
    # ..And use region, because Fog chokes on post with region redirects
    aws_bucket = Fog::Storage.new({
      path_style: true,
      provider: 'AWS',
      aws_access_key_id: $aws['access_key'],
      aws_secret_access_key: $aws['secret_key'],
      region: 'us-west-1'
    }).directories.get(
      bucket
    )

    raise "S3 bucket missing: #{bucket}" unless aws_bucket

    fog_file = aws_bucket.files.create(
      key: key,
      body: File.open(source),
      public: true
    )

    return fog_file.url(Date.tomorrow.to_time.to_i)[/.+\?/][0..-2]
  end

  task :send_report, [:arg1] => :environment do |t, args|
    log_fn = './log/import.log'

    str = File.read(log_fn)
    success = true
    success = false if str.match(/rake aborted!/) || !str.match(/Events: \d+/)

    if success
      subject = "Tableau import succeeded"
    else
      subject = "Tableau import FAILED"
    end

    log_excerpt = ""
    arr = str.slice(-10000, 10000).split(/\r*\n/)
    arr.each do |line|
      log_excerpt += " " + line + "\n"  # We have to put the space in front of each line, or sendgrid will convert the email to html for mysterious reasons
    end

    today_str = Time.now.beginning_of_day.strftime("%Y-%m-%d")
    aws_key = (ENV['IMPORT_START'] || today_str)
    aws_key = "EventsFrom_" + aws_key + "_-_ImportedOn_" + today_str + ".log"
    url = upload_s3 $aws['bucket'], aws_key, log_fn, false
    text = "See full log file at:

#{url}

Last 10k of log:

#{log_excerpt}
"

    subject = subject.gsub(/"/, "1")
    text = text.gsub(/"/, "``").gsub(/`/, "'")

    log_output "Import log: \n#{str}"

    email_command = %Q{
curl https://api.sendgrid.com/api/mail.send.json \
-F to="#{$to}" \
-F cc="#{$cc}" \
-F toname="#{$toname}" \
-F subject="#{subject}" \
-F text="#{text}" \
-F from="#{$from}" \
-F api_user=#{$sendgrid_username} \
-F api_key=#{$sendgrid_password}
}

    result = `#{email_command}`
    log_output "\n\nResult: #{result}\n\n"
    if result == '{"message":"success"}'
      # Engtools has been notified successfully
    else
      Rollbar.report_message("4d studio ETL import cannot send email! Response: #{result}", "error")
    end    
  end

end
