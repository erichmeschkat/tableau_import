     CREATE TABLE "fact_app_event"
     (
         "event_id"              BIGSERIAL PRIMARY KEY,
         "event_time"            TIMESTAMP,
         "dim_date_id_event"     INT      ,
         "dim_event_type_id"     INT      ,
         "dim_application_id"    INT      ,
         "dim_app_version_id"    INT      ,
         "dim_device_id"  INT      ,
         "dim_session_id"        INT      ,
         "dim_analytics_version_id" INT   ,
         "session_counter"       INT      ,
         "latitude"              REAL     ,
         "longitude"             REAL     ,
         "dim_carrier_id"        INT      ,
         "old_version_id"        INT      ,
         "new_version_id"        INT      ,
         "dim_location_id"       INT      ,
         "dim_device_os_version_id" INT      ,
         "dim_ip_address_id"     INT      ,
         "collect_time"          TIMESTAMP,
         "dim_date_id_collect"   INT,
         "dim_time_zone_id"   INT
     );

     CREATE TABLE "fact_special_event"
     (
         "event_id"                   BIGSERIAL PRIMARY KEY,
         "event_time"                 TIMESTAMP,
         "dim_date_id_event"          INT      ,
         "dim_event_type_id"          INT      ,
         "dim_application_id"         INT      ,
         "dim_app_version_id"         INT      ,
         "dim_device_id"       INT      ,
         "dim_session_id"             INT      ,
         "dim_analytics_version_id" INT   ,
         "session_counter"            INT      ,
         "latitude"                   REAL     ,
         "longitude"                  REAL     ,
         "duration"                   INT      ,
         "target_acquisition_counter" INT      ,
         "dim_content_id"                 INT      ,
         "dim_project_id"                  INT      ,
         "user_experience_counter"    INT      ,
         "dim_experience_id"          INT      ,
         "autoplay"                   INT      ,
         "dim_reason_ended_id"        INT      ,
         "outside_activity_counter"   INT      ,
         "dim_outside_activity_id"    INT      ,
         "dim_button_id"              INT      ,
         "dim_card_id"                INT      ,
         "dim_share_service_id"       INT      ,
         "dim_location_id"            INT      ,
         "dim_ip_address_id"          INT      ,
         "dim_share_type_id"          INT      ,
         "collect_time"               TIMESTAMP,
         "dim_date_id_collect"        INT      ,
         "share_counter"              INT,
         "dim_time_zone_id"   INT,
         "dim_url_id" INT,
         "dim_activity_type_id" INT,
         "dim_video_service_id" INT,
         "dim_video_type_id" INT
     );

     CREATE TABLE "fact_standard_event"
     (
         "event_id"                   BIGSERIAL PRIMARY KEY,
         "event_time"                 TIMESTAMP,
         "dim_date_id_event"          INT      ,
         "dim_event_type_id"          INT      ,
         "dim_application_id"         INT      ,
         "dim_app_version_id"         INT      ,
         "dim_device_id"       INT      ,
         "dim_session_id"             INT      ,
         "dim_analytics_version_id" INT   ,
         "session_counter"            INT      ,
         "latitude"                   REAL     ,
         "longitude"                  REAL     ,
         "dim_network_id"             INT      ,
         "duration"                   INT      ,
         "target_acquisition_counter" INT      ,
         "dim_content_id"                 INT      ,
         "dim_project_id"                  INT      ,
         "user_experience_counter"    INT      ,
         "dim_experience_id"          INT      ,
         "initiating_chapter"         INT      ,
         "destination_chapter"        INT      ,
         "dim_reason_ended_id"        INT      ,
         "dim_location_id"            INT      ,
         "dim_ip_address_id"          INT      ,
         "collect_time"               TIMESTAMP,
         "dim_date_id_collect"        INT,
         "dim_time_zone_id"   INT
     );

     CREATE TABLE "fact_error_event"
     (
         "event_id"             BIGSERIAL     PRIMARY KEY,
         "event_time"           TIMESTAMP    ,
         "dim_date_id_event"    INT          ,
         "dim_event_type_id"    INT          ,
         "dim_application_id"   INT          ,
         "dim_app_version_id"   INT          ,
         "dim_device_id" INT          ,
         "dim_session_id"       INT          ,
         "dim_analytics_version_id" INT   ,
         "session_counter"      INT          ,
         "latitude"             REAL         ,
         "longitude"            REAL         ,
         "dim_error_id"         INT          ,
         "dim_location_id"      INT          ,
         "dim_ip_address_id"    INT          ,
         "collect_time"         TIMESTAMP    ,
         "dim_date_id_collect"  INT,
         "dim_time_zone_id"   INT
     );

     CREATE TABLE fact_purchase_event
     (
       "event_id" bigserial  PRIMARY KEY,
       "event_time" timestamp without time zone,
       "dim_date_id_event" integer,
       "dim_event_type_id" integer,
       "dim_application_id" integer,
       "dim_app_version_id" integer,
       "dim_device_id" integer,
       "dim_session_id" integer,
       "dim_analytics_version_id" integer,
       "session_counter" integer,
       "latitude" real,
       "longitude" real,
       "dim_network_id" integer,
       "purchased_content" character varying(45),
       "currency" character varying(10),
       "price_usd" double precision,
       "dim_location_id" integer,
       "dim_ip_address_id" integer,
       "collect_time" timestamp without time zone,
       "dim_date_id_collect" integer,
       "dim_time_zone_id" integer,
     );

     CREATE INDEX fact_app_event_collect_time ON fact_app_event (collect_time);
     CREATE INDEX fact_special_event_collect_time ON fact_special_event (collect_time);
     CREATE INDEX fact_standard_event_collect_time ON fact_standard_event (collect_time);
     CREATE INDEX fact_error_event_collect_time ON fact_error_event (collect_time);
     CREATE INDEX fact_purchase_event ON fact_purchase_event (collect_time);

     CREATE UNIQUE INDEX dim_activity_type_id ON dim_activity_type ( dim_activity_type_id );
     CREATE UNIQUE INDEX dim_analytics_version_id ON dim_analytics_version ( dim_analytics_version_id );
     CREATE UNIQUE INDEX dim_app_version_id ON dim_app_version ( dim_app_version_id );
     CREATE UNIQUE INDEX dim_application_id ON dim_application ( dim_application_id );
     CREATE UNIQUE INDEX dim_button_id ON dim_button ( dim_button_id );
     CREATE UNIQUE INDEX dim_card_id ON dim_card ( dim_card_id );
     CREATE UNIQUE INDEX dim_carrier_id ON dim_carrier ( dim_carrier_id );
     CREATE UNIQUE INDEX dim_content_id ON dim_content ( dim_content_id );
     CREATE UNIQUE INDEX dim_date_id ON dim_date ( dim_date_id );
     CREATE UNIQUE INDEX dim_device_id ON dim_device ( dim_device_id );
     CREATE UNIQUE INDEX dim_device_app_interaction_id ON dim_device_app_interaction ( dim_device_app_interaction_id );
     CREATE UNIQUE INDEX dim_device_os_version_id ON dim_device_os_version ( dim_device_os_version_id );
     CREATE UNIQUE INDEX dim_enterprise_id ON dim_enterprise ( dim_enterprise_id );
     CREATE UNIQUE INDEX dim_error_id ON dim_error ( dim_error_id );
     CREATE UNIQUE INDEX dim_event_type_id ON dim_event_type ( dim_event_type_id );
     CREATE UNIQUE INDEX dim_experience_id ON dim_experience ( dim_experience_id );
     CREATE UNIQUE INDEX dim_project_id ON dim_project ( dim_project_id );
     CREATE UNIQUE INDEX dim_ip_address_id ON dim_ip_address ( dim_ip_address_id );
     CREATE UNIQUE INDEX dim_location_id ON dim_location ( dim_location_id );
     CREATE UNIQUE INDEX dim_network_id ON dim_network ( dim_network_id );
     CREATE UNIQUE INDEX dim_outside_activity_id ON dim_outside_activity ( dim_outside_activity_id );
     CREATE UNIQUE INDEX dim_reason_ended_id ON dim_reason_ended ( dim_reason_ended_id );
     CREATE UNIQUE INDEX dim_session_id ON dim_session ( dim_session_id );
     CREATE UNIQUE INDEX dim_share_service_id ON dim_share_service ( dim_share_service_id );
     CREATE UNIQUE INDEX dim_share_type_id ON dim_share_type ( dim_share_type_id );
     CREATE UNIQUE INDEX dim_time_zone_id ON dim_time_zone ( dim_time_zone_id );
     CREATE UNIQUE INDEX dim_url_id ON dim_url ( dim_url_id );
     CREATE UNIQUE INDEX dim_user_id ON dim_user ( dim_user_id );
     CREATE UNIQUE INDEX dim_video_service_id ON dim_video_service ( dim_video_service_id );
     CREATE UNIQUE INDEX dim_video_type_id ON dim_video_type ( dim_video_type_id );
