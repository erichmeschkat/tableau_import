
      CREATE TABLE "dim_analytics_version"
      (
          "dim_analytics_version_id" SERIAL       PRIMARY KEY,
          "analytics_version"        VARCHAR (45)
      );

      CREATE TABLE "dim_app_version"
      (
          "dim_app_version_id" SERIAL PRIMARY KEY,
          "dim_application_id"     INT         ,
          "version"            VARCHAR (45)
      );

      CREATE TABLE "dim_application"
      (
          "dim_application_id"          INT          PRIMARY KEY,
          "application_brand"           VARCHAR (45),
          "application_current_version" VARCHAR (10),
          "enterprise_id"               INT,
          "image_url"           VARCHAR(100),
          "application_name"    VARCHAR(100)
      );

      CREATE TABLE "dim_button"
      (
          "dim_button_id" SERIAL       PRIMARY KEY,
          "button_type"   VARCHAR (45),
          "button_name"   VARCHAR (45)
      );

      CREATE TABLE "dim_card"
      (
          "dim_card_id" INT           PRIMARY KEY,
          "dim_project_id" INT,
          "dim_content_id" INT,
          "dim_experience_id" INT,
          "dim_application_id" INT,
          "card_url"    VARCHAR (100),
          "card_type"   VARCHAR (20),
          "card_title"  VARCHAR(100)
      );

      CREATE TABLE "dim_user"
      (
          "dim_user_id" INT           PRIMARY KEY,
          "name" VARCHAR(200),
          "title" VARCHAR(200),
          "department" VARCHAR(200),
          "phone" VARCHAR(200),
          "dim_enterprise_id"    INT,
          "created_at"  DATE,
          "updated_at" DATE,
          "avatar" VARCHAR(200),
          "dim_project_id" INT,
          "created_by" INT,
          "eula_accepted" INT,
          "sign_in_count" INT,
          "preferences" TEXT
      );

      CREATE TABLE "dim_carrier"
      (
          "dim_carrier_id" SERIAL       PRIMARY KEY,
          "carrier_name"   VARCHAR (45)
      );

      CREATE TABLE "dim_content"
      (
          "dim_content_id" INT          PRIMARY KEY,
          "target_id"      INT         ,
          "content_name"   VARCHAR (200),
          "path"           VARCHAR (45),
          "vws_path"       VARCHAR (45),
          "dim_project_id"      INT,
          "rating" INT,
          "thumbnail_url"  VARCHAR(200),
          "target_img_url"  VARCHAR(200),
          "extended_tracking" CHAR(1),
          "template_type"  VARCHAR(45),
          "num_chapters" INT,
          "num_experiences" INT,
          "dim_enterprise_id" INT,
          "dim_application_id" INT,
          "target_image_url_thumb" VARCHAR(255),
          "target_image_url_large" VARCHAR(255)
      );


      CREATE TABLE "dim_date"
      (
          "dim_date_id"            INT         PRIMARY KEY,
          "full_date"              DATE       ,
          "day_of_week"            SMALLINT   ,
          "day_num_in_month"       SMALLINT   ,
          "day_num_overall"        SMALLINT   ,
          "day_name"               VARCHAR (9),
          "day_abbrev"             CHAR (3)   ,
          "weekday_flag"           CHAR (1)   ,
          "week_num_in_year"       SMALLINT   ,
          "week_num_overall"       SMALLINT   ,
          "week_begin_date"        DATE       ,
          "week_begin_date_key"    INT        ,
          "month"                  SMALLINT   ,
          "month_num_overall"      SMALLINT   ,
          "month_name"             VARCHAR (9),
          "month_abbrev"           CHAR (3)   ,
          "quarter"                SMALLINT   ,
          "year"                   SMALLINT   ,
          "yearmo"                 INT        ,
          "fiscal_month"           SMALLINT   ,
          "fiscal_quarter"         SMALLINT   ,
          "fiscal_year"            SMALLINT   ,
          "last_day_in_month_flag" CHAR (1)   ,
          "same_day_year_ago_date" DATE
      );

      CREATE TABLE "dim_device_app_interaction"
      (
        "dim_device_app_interaction_id" BIGSERIAL PRIMARY KEY,
        "dim_device_id" INT NOT NULL,
        "dim_application_id" INT NOT NULL,
        "first_seen" TIMESTAMP,
        "last_seen" TIMESTAMP
      );

      CREATE TABLE "dim_device_os_version"
      (
          "dim_device_os_version_id" SERIAL       PRIMARY KEY,
          "device_os_version"                  VARCHAR (100)
      );

      CREATE TABLE "dim_device"
      (
          "dim_device_id" BIGSERIAL     PRIMARY KEY,
          "distinct_id"          VARCHAR (255),
          "device_model"         VARCHAR (45) ,
          "advertiser_id"        INT          ,
          "vendor_id"            VARCHAR (100) ,
          "device_os"            VARCHAR (100),
          "language"             VARCHAR (45) ,
          "device_type"          VARCHAR (45)
      );

      CREATE TABLE "dim_enterprise"
      (
          "dim_enterprise_id" INT          PRIMARY KEY,
          "enterprise_name"   VARCHAR (45),
          "tier" INT,
          "seats" INT,
          "filled_seats" INT,
          "storage_current_usage" BIGINT,
          "tableau_username" VARCHAR(100)
      );

      CREATE TABLE "dim_event_type"
      (
          "dim_event_type_id" SERIAL          PRIMARY KEY,
          "event_type"        VARCHAR (45)
      );

      CREATE TABLE "dim_error"
      (
          "dim_error_id"       SERIAL PRIMARY KEY,
          "error_code"           VARCHAR (45),
          "error_name"           VARCHAR (255)
      );

      CREATE TABLE dim_experience
      (
          dim_experience_id SERIAL          PRIMARY KEY,
          experience_id     VARCHAR (45) ,
          dim_content_id    INT           NOT NULL,
          dim_application_id INT,
          dim_project_id INT,
          dim_enterprise_id INT,
          experience_name   VARCHAR (150),
          experience_type   VARCHAR (45) ,
          experience_path   VARCHAR (45) ,
          experience_image_url    VARCHAR (500),
          experience_video_url    VARCHAR (500)
      );

      CREATE TABLE "dim_project"
      (
          "dim_project_id" INT          PRIMARY KEY,
          "project_name"   VARCHAR (150),
          "live_date"     VARCHAR (45) ,
          "dim_enterprise_id" INT,
          "dim_application_id" INT
      );

      CREATE TABLE "dim_ip_address"
      (
          "dim_ip_address_id" BIGSERIAL PRIMARY KEY,
          "ip_address"   VARCHAR (45)
      );

      CREATE TABLE "dim_location"
      (
          "dim_location_id" BIGSERIAL    PRIMARY KEY,
          "city"            VARCHAR (45),
          "state"           VARCHAR (45),
          "country"         VARCHAR (45)
      );

      CREATE TABLE "dim_network"
      (
          "dim_network_id" SERIAL       PRIMARY KEY,
          "network"        VARCHAR (45)
      );

      CREATE TABLE "dim_outside_activity"
      (
          "dim_outside_activity_id" SERIAL        PRIMARY KEY,
          "outside_activity_type"   VARCHAR (45) ,
          "outside_activity_name"   VARCHAR (200),
          "outside_activity_url"    VARCHAR (100)
      );

      CREATE TABLE "dim_reason_ended"
      (
          "dim_reason_ended_id" SERIAL       PRIMARY KEY,
          "reason_ended"        VARCHAR (45)
      );

      CREATE TABLE "dim_session"
      (
          "dim_session_id"       BIGSERIAL PRIMARY KEY,
          "dim_device_id" INT      ,
          "session_counter"      INT
      );

      CREATE TABLE "dim_share_service"
      (
          "dim_share_service_id" SERIAL       PRIMARY KEY,
          "share_service"        VARCHAR (45)
      );

      CREATE TABLE "dim_share_type"
      (
          "dim_share_type_id" SERIAL       PRIMARY KEY,
          "share_type"        VARCHAR (45)
      );

      CREATE TABLE "dim_time_zone"
      (
          "dim_time_zone_id" SERIAL       PRIMARY KEY,
          "time_zone"        VARCHAR (45)
      );

      CREATE TABLE "dim_url"
      (
          "dim_url_id" SERIAL       PRIMARY KEY,
          "url"        VARCHAR (255)
      );

      CREATE TABLE "dim_video_type"
      (
          "dim_video_type_id" SERIAL       PRIMARY KEY,
          "video_type"        VARCHAR (100)
      );

      CREATE TABLE "dim_video_service"
      (
          "dim_video_service_id" SERIAL       PRIMARY KEY,
          "video_service"        VARCHAR (100)
      );

      CREATE TABLE "dim_activity_type"
      (
          "dim_activity_type_id" SERIAL       PRIMARY KEY,
          "activity_type"        VARCHAR (100)
      );