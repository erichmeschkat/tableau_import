class HomeController < ApplicationController
  require 'mixpanel_client'

  def index

  end

  def import
    start = params[:date]
    en = params[:date]
    cmd = "RAILS_ENV=#{Rails.env} IMPORT_START=#{start} IMPORT_END=#{en} /bin/bash ./import.sh"
    log = `#{cmd}`
    @log = log
  end

  def emailcsv
    start = params[:date]
    en = params[:date]
    cmd = "RAILS_ENV=#{Rails.env} IMPORT_START=#{start} IMPORT_END=#{en} rake tableau:emailcsv"
    log = `#{cmd}`
    @log = log    
  end
end
