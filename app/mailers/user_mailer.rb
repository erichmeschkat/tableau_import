class UserMailer < ActionMailer::Base
	default from: 'analytics@daqri.com'
 
  def analytics_email(email, url, app_name)
    mail(to: email, body: "Report saved at: #{url}", content_type: "text/html", subject: "Daily Analytics Report - #{app_name}")
  end
end